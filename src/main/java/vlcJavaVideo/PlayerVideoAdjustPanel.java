/*
 * This file is part of VLCJ.
 *
 * VLCJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VLCJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLCJ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009-2016 Caprica Software Limited.
 */

package vlcJavaVideo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import uk.co.caprica.vlcj.binding.LibVlcConst;
import uk.co.caprica.vlcj.player.MediaPlayer;

public class PlayerVideoAdjustPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private final MediaPlayer mediaPlayer;



    private JLabel brightnessLabel;
    private JSlider brightnessSlider;

    private JLabel hueLabel;
    private JSlider hueSlider;

    private JLabel saturationLabel;
    private JSlider saturationSlider;

    private JLabel gammaLabel;
    private JSlider gammaSlider;

    public PlayerVideoAdjustPanel(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;

        createUI();
    }

    private void createUI() {
        layoutControls();
    }



    private void layoutControls() {
        setBorder(new EmptyBorder(4, 4, 4, 4));

        setLayout(new BorderLayout());


    }


}
